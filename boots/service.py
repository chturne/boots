#!/usr/bin/env python3

from datetime import datetime
from collections import namedtuple
from pathlib import Path
from urllib.parse import urlparse
import fcntl
import functools
from flask import (
    Flask, jsonify, current_app, request, url_for
)
import io
import ipaddress
import jinja2
import os
import re
import requests
import shutil
import subprocess
import tempfile
import threading
import time
import uuid
from pprint import pformat
from logging import getLogger, getLevelName, Formatter, StreamHandler

logger = getLogger(__name__)
logger.setLevel(getLevelName('DEBUG'))
log_formatter = \
    Formatter("%(asctime)s [%(levelname)s] %(funcName)s: "
              "%(message)s [%(threadName)s] ")
console_handler = StreamHandler()
console_handler.setFormatter(log_formatter)
logger.addHandler(console_handler)


BASE_DIR = os.path.dirname(__file__)

app = Flask(__name__,
            instance_relative_config=True)

app.config.from_mapping(
    CONFIG_ROOT=Path('/boots'),
    MARS_HOST=os.getenv('MARS_HOST', None),
)


class InvalidUsage(Exception):
    def __init__(self, message, status_code=400, payload=None):
        Exception.__init__(self)
        self.message = message
        self.status_code = status_code
        self.payload = payload

    def __str__(self):  # pragma: no cover
        return f"{self.status_code}: {self.message}"

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(BASE_DIR, 'templates')),
    extensions=['jinja2.ext.autoescape'],
    autoescape=False,
    trim_blocks=True,
    lstrip_blocks=True)


def mars_fetch_machines(mars_host):  # pragma: nocover
    logger.info(f"Fetching machines from MaRS at {mars_host}")
    while True:
        try:
            r = requests.get(
                f'{mars_host}/api/v1/machine/')
            r.raise_for_status()
            return r.json()
        except requests.ConnectionError as err:
            logger.info(f"Initial machine fetch from MaRS ({mars_host}) "
                        f"has failed. Error: {err}. "
                        "Trying again...")
            time.sleep(10)


def configure_network_for_machine(app, machine):  # pragma: nocover
    with app.app_context():
        stamp_dhcp(dnsmasq_dhcp_hosts_file(),
                   machine['mac_address'],
                   machine['ip_address'],
                   machine['full_name'])


def removesuffix(s, suffix):
    # FIXME: remove me in py3.9
    # suffix='' should not call self[:-0].
    if suffix and s.endswith(suffix):
        return s[:-len(suffix)]
    else:
        return s[:]


def parse_iso8601_date(d):
    # For some reason the datetime parser doesn't like the lone Z, I
    # thought it was spec'd.
    return datetime.fromisoformat(removesuffix(d, "Z"))


def process_mars_events(app, events):  # pragma: nocover
    logger.debug(f"===== events since last fetch =====\n:{pformat(events)}")
    try:
        for event in events:
            last_checked = parse_iso8601_date(event['date'])
            r = requests.get(event['machine'])
            r.raise_for_status()
            machine = r.json()
            logger.debug(f"configuring {machine['mac_address']} for event {event}")
            configure_network_for_machine(app, machine)
        return last_checked
    except requests.ConnectionError as err:
        logger.warn(f"connection error, bailing early: {err}")
        return last_checked


def mars_poller(app, mars_host):  # pragma: nocover
    logger.info("Running the MaRS poll thread...")
    machines = mars_fetch_machines(mars_host)
    if machines:
        logger.info(f"configuring the default network for {len(machines)} machines...")
        for machine in machines:
            logger.debug(f"configuring {machine['mac_address']}...")
            configure_network_for_machine(app, machine)

    last_checked = datetime.now()
    logger.info("machine initialization complete, polling for events...")
    while True:
        try:
            r = requests.get(
                f'{mars_host}/api/v1/events/?since={last_checked.isoformat()}')
            r.raise_for_status()
            events = r.json()
            if events:
                last_checked = process_mars_events(app, events)
        except requests.ConnectionError:
            pass
        finally:
            time.sleep(5)


def render_template(tmpl_name, **options):
    return JINJA_ENVIRONMENT.get_template(tmpl_name).render(**options)


def split_mac_addr(s):
    """Return an array of bytes for the mac address in string s. This
    method is flexible in its input."""
    s = s.lower()
    m = re.match("[0-9a-f]{2}([-:]?)[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$",
                 s)
    if not m:
        raise ValueError(f"{s} is not a valid mac address")
    delim = m.groups()[0]
    if delim:
        return s.split(delim)
    else:
        return [s[i:i+2] for i in range(0, len(s), 2)]


def boots_root():
    return current_app.config['CONFIG_ROOT']


def resolve_nfs_dir(filesystem_path):
    assert filesystem_path
    if not filesystem_path.startswith("/"):
        filesystem_path = "/" + filesystem_path
    return (boots_root() / 'nfs-roots' /
            Path(filesystem_path).relative_to("/"))


# This is a mapping from the "external name" like "/brian" to the
# "internal" name, like "/mnt/tmp/boots/tftp/brian"
def resolve_kernel_filename(kernel_path):
    assert kernel_path
    if not kernel_path.startswith("/"):
        kernel_path = "/" + kernel_path
    return (boots_root() / 'tftp' /
            Path(kernel_path).relative_to("/"))


def dnsmasq_options_file():
    return boots_root() / 'options.dhcp'


def dnsmasq_dhcp_hosts_file():
    return boots_root() / 'hosts.dhcp'


def pxelinux_root():
    return boots_root() / 'tftp' / 'pxelinux.cfg'


def encode_pxelinux_filename(mac_addr):
    if isinstance(mac_addr, str):
        mac_addr = split_mac_addr(mac_addr)
    assert isinstance(mac_addr, list) and \
        len(mac_addr) == 6
    return "01-" + '-'.join(mac_addr)


def resolve_pxelinux_filename(mac_addr):
    if mac_addr == "default":
        filename = "default"
    else:
        filename = encode_pxelinux_filename(mac_addr)

    return pxelinux_root() / filename


def stamp_pxelinux_nfs(pxelinux_filename,
                       nfs_server_ip,
                       kernel_filesystem_path,
                       root_filesystem_path,
                       initrd_path=None,
                       kernel_cmdline_extras=None):
    result = (render_template(
        'pxelinux-nfs.jinja',
        kernel_path=kernel_filesystem_path,
        root_filesystem_path=root_filesystem_path,
        nfs_server_ip=nfs_server_ip,
        initrd_path=initrd_path,
        kernel_cmdline_extras=kernel_cmdline_extras))
    with open(pxelinux_filename, 'w') as config:
        fcntl.flock(config, fcntl.LOCK_EX)
        try:
            config.write(result)
        finally:
            fcntl.flock(config, fcntl.LOCK_UN)


def stamp_pxelinux_b2c(pxelinux_filename,
                       kernel_filesystem_path,
                       containers,
                       initrd_path=None,
                       kernel_cmdline_extras=None):
    result = (render_template(
        'pxelinux-b2c.jinja',
        kernel_path=kernel_filesystem_path,
        initrd_path=initrd_path,
        containers=containers,
        kernel_cmdline_extras=kernel_cmdline_extras))
    with open(pxelinux_filename, 'w') as config:
        fcntl.flock(config, fcntl.LOCK_EX)
        try:
            config.write(result)
        finally:
            fcntl.flock(config, fcntl.LOCK_UN)


def stamp_pxelinux_raw(pxelinux_filename,
                       kernel_path,
                       cmdline,
                       initrd_path=None):
    result = (render_template(
        'pxelinux-raw.jinja',
        kernel_path=kernel_path,
        cmdline=cmdline,
        initrd_path=initrd_path))

    with open(pxelinux_filename, 'w') as config:
        fcntl.flock(config, fcntl.LOCK_EX)
        try:
            config.write(result)
        finally:
            fcntl.flock(config, fcntl.LOCK_UN)


Host = namedtuple('Host', ['mac_addr', 'ip_addr', 'hostname'])


def parse_dhcp_hosts(s):
    hosts = []
    for host_desc in s.splitlines():
        host_desc = host_desc.strip()
        if not len(host_desc) or host_desc.startswith("#"):
            continue
        m = re.match(
            r'^(?P<mac>[^,]+),(?P<ip>[^,]+),set:(?P<hostname>.+)$',
            host_desc)
        if not m:
            raise InvalidUsage("Malformed DHCP hosts file")
        hosts.append(Host(*m.groups()))
    return hosts


def reload_dnsmasq():  # pragma: no cover
    logger.debug("reloading dnsmasq...")
    try:
        with open('/var/run/dnsmasq.pid', 'r') as f:
            pid = int(f.read())
        subprocess.check_call(["kill", "-SIGHUP", str(pid)])
    except FileNotFoundError:
        logger.error("dnsmasq.pid not found, is it running?")


def stamp_dhcp(hosts_file, mac_addr, ip_addr, hostname):
    logger.debug("setting DHCP configuration for %s", mac_addr)

    mac_addr = ':'.join(split_mac_addr(mac_addr))

    def create_if_not_exists(filename):
        if not os.path.exists(filename):
            with open(filename, 'w') as _:
                pass

    create_if_not_exists(hosts_file)
    with open(hosts_file, 'r+') as f:
        fcntl.flock(f, fcntl.LOCK_EX)
        try:
            machines = parse_dhcp_hosts(f.read())
            new_machines = [m for m in machines if m.mac_addr != mac_addr]
            new_machines.append(Host(mac_addr, ip_addr, hostname))
            f.seek(0)
            config = render_template(
                'dnsmasq-dhcp-host.jinja',
                machines=sorted(new_machines, key=lambda m: m[0]))
            f.write(config)
            f.truncate()
            # If something kills the server right now, we'll have
            # a stale lock, I guess bootup needs to take that into
            # account.
        finally:
            fcntl.flock(f, fcntl.LOCK_UN)
            reload_dnsmasq()


TASKS = {}
rlock = threading.RLock()


def new_task(msg):
    task_id = uuid.uuid4().hex
    task = {"state": "pending",
            "link": '{}{}'.format(
                request.url_root[:-1],
                url_for('check_task', task_id=task_id)),
            "task_id": task_id,
            "message": msg}
    return task_id, task


def dispatch_task(f, pending_msg):
    global TASKS
    task_id, task = new_task(pending_msg)
    with rlock:
        TASKS[task_id] = task
    t = threading.Thread(
        target=f,
        daemon=True,
        kwargs={"boots_root": boots_root(),
                "task": task}
    )
    t.start()
    return task


def clean_old_tasks():  # pragma: no cover
    """
    This function cleans up old tasks from our in-memory data structure.
    """
    global TASKS
    while True:
        logger.debug("Waking up to clean old tasks...")
        # Only keep tasks that are running or that finished less than 5
        # minutes ago.
        five_min_ago = datetime.timestamp(datetime.utcnow()) - 5 * 60
        with rlock:
            TASKS = {task_id: task for task_id, task in TASKS.items()
                     if 'completion_timestamp' not in task or
                     task['completion_timestamp'] > five_min_ago}
        time.sleep(60)


def _download(src, dst, boots_root, task, fn):
    """Locks are not taken to store into what is essentially the
    global TASKS table here (indirectly via _task_), thanks to the
    CPython GIL.

    The dictionary writes under-the-hood are single STORE_SUBSCR ops,
    and hence they are atomic, so no "meta-stable" reads are possible
    from request threads.

    It won't take much more hair before we should bite the bullet and
    use something like Redis for this"""
    logger.debug("_download %s -> %s", src, dst)

    remote_filename = os.path.basename(urlparse(src).path)
    with tempfile.TemporaryDirectory(dir=boots_root) as tmpdirname:
        output = os.path.join(tmpdirname, remote_filename)
        try:
            with open(output, 'wb') as f:
                r = requests.get(src, stream=True)
                if 'content-length' in r.headers:
                    task['content-length'] = r.headers['content-length']
                task['content-downloaded'] = 0
                for block in r.iter_content(io.DEFAULT_BUFFER_SIZE):
                    f.write(block)
                    task['content-downloaded'] += len(block)
            fn(output, dst)
            task['state'] = 'finished'
        except (requests.exceptions.ConnectionError,
                requests.exceptions.HTTPError) as e:
            task['state'] = 'failed'
            task['message'] = str(e)
        except Exception as e:
            task['state'] = 'failed'
            task['message'] = str(e)


def unpack_archive(archive, target, cache_key=None):
    # Note: shutil.unpack archive will enter an infinite loop if you
    # try to overwrite an existing filesystem.  POSIX offers no way to
    # do a race free overwrite of symlinks, and Python's os.symlink
    # API by design offer no fancy race-free "force" versions of
    # symlink. shutil's unpack_archive offers no solutions either. So
    # instead monkeypatch the os.symlink to ignore existing files.
    # https://bugs.python.org/issue36656 A solution will be available
    # when https://github.com/python/cpython/pull/14464 lands, but for
    # now, simply remove the existing archive.  You can monkey patch
    # os.symlink and ignore existent files, but a) what if you do want
    # to change it... and b) os.mknod suffers the same problem, so be
    # heavy handed and move on! If clients are currently using this
    # filesystem, then tough, that is the clients mistake for
    # overwriting it. There is a flag to check for this, if they want.
    def debug_show_errs(function, path, excinfo):  # pragma: no cover
        logger.error("rmtree errror: {function} {path} {excinfo}")

    shutil.rmtree(target, ignore_errors=True, onerror=debug_show_errs)
    shutil.unpack_archive(archive, target)
    if cache_key:
        cache_key_path = target / cache_key
        if cache_key_path.exists():
            raise InvalidUsage(f"{cache_key_path} already exists in archive")
        cache_key_path.touch()


def download_and_unpack(src, dst, boots_root, task, cache_key):
    logger.debug("download_and_unpack")

    if dst.is_dir() and (dst / cache_key).exists():
        task["message"] = f"already downloaded {cache_key}"
        task["state"] = "finished"
        return
    cached_unpack_archive = \
        functools.partial(unpack_archive, cache_key=cache_key)
    _download(src, dst, boots_root, task, cached_unpack_archive)


def download(src, dst, boots_root, task):
    _download(src, dst, boots_root, task, os.rename)


@app.route('/tasks/<task_id>', methods=["GET"])
def check_task(task_id):
    with rlock:
        if task_id not in TASKS:
            return {"message": f"task {task_id} is not running"}, 404

        task = TASKS[task_id]
        if task["state"] == "finished":
            return task, 303
        else:
            # Clients must parse the body to find out task state
            return task, 200


@app.route('/kernels/<name>', methods=["GET"])
def check_kernel(name):
    if resolve_kernel_filename(name).exists():
        return {}
    return {"message": "does not exist"}, 404


@app.route('/filesystems/<name>', methods=["GET"])
def check_filesystem(name):
    if resolve_nfs_dir(name).is_dir():
        # FIXME: Better sanity checking
        return {}
    else:
        return {"message": "is not a directory"}, 404


def parse_download_request(req):
    required_params = ["name",
                       "path"]
    for required in required_params:
        if required not in req:
            raise InvalidUsage(f"{required} is a required parameter")
    req["overwrite"] = "overwrite" in req
    return req


def parse_filesystem_download_request(req):
    req = parse_download_request(req)
    if "cache-key" not in req:
        raise InvalidUsage("cache_key is a required parameter")
    return req


@app.route('/filesystems/download', methods=["POST"])
def download_filesystem():
    if request.method == 'POST':
        req = parse_filesystem_download_request(request.get_json())
        filename = resolve_nfs_dir(req["name"])
        if filename.exists() and not req["overwrite"]:
            raise InvalidUsage(f'{req["name"]} already exists')
        f = functools.partial(download_and_unpack,
                              req["path"],
                              filename,
                              cache_key=req["cache-key"])
        return dispatch_task(f,
                             "downloading a filesystem"), 202


@app.route('/kernels/download', methods=["POST"])
@app.route('/ramdisks/download', methods=["POST"])
def download_kernel():
    if request.method == 'POST':
        req = parse_download_request(request.get_json())
        filename = resolve_kernel_filename(req["name"])
        if filename.exists() and not req["overwrite"]:
            raise InvalidUsage(f'{req["name"]} already exists')
        f = functools.partial(download,
                              req["path"],
                              filename)
        return dispatch_task(f,
                             "downloading a kernel"), 202


def parse_network_update_request(req):
    for required in ["ip", "hostname"]:
        if required not in req:
            raise InvalidUsage(f"{required} is a required parameter")
    try:
        return ipaddress.ip_address(req['ip']), req['hostname']
    except ValueError as e:  # pragma: nocover
        raise InvalidUsage(str(e)) from e


def parse_boot_configuration_request(req):
    return {
        'kernel_path': req.get('kernel_path', os.environ.get('KERNEL_PATH')),
        'initrd_path': req.get('initrd_path', os.environ.get('INITRD_PATH')),
        'cmdline': req['cmdline'],
    }


def set_network_configuration_for_request(mac_addr, ip, hostname):
    logger.info("%s -> (%s, %s)", mac_addr, ip, hostname)
    stamp_dhcp(dnsmasq_dhcp_hosts_file(),
               mac_addr,
               ip,
               hostname)


@app.route('/duts/<mac_addr>/network', methods=['POST'])
def set_dut_network_configuration(mac_addr):  # pragma: nocover
    if request.method == 'POST':
        ip, hostname = parse_network_update_request(request.get_json())
        set_network_configuration_for_request(mac_addr, ip=ip, hostname=hostname)
    return {}


@app.route('/duts/<mac_addr>/boot', methods=['POST', 'DELETE'])
def dut_boot_configuration(mac_addr):
    if request.method == 'POST':
        params = parse_boot_configuration_request(request.get_json())
        stamp_pxelinux_raw(resolve_pxelinux_filename(mac_addr),
                           **params)
        return {}

    if request.method == 'DELETE':
        filename = resolve_pxelinux_filename(mac_addr)
        if not os.path.exists(filename):
            return {}, 404
        os.remove(filename)
        return {}


@app.route('/duts/<mac_addr>', methods=['POST'])
def create_dut(mac_addr):
    if request.method == 'POST':
        r = request.get_json()
        params = parse_boot_configuration_request(r)
        stamp_pxelinux_raw(resolve_pxelinux_filename(mac_addr),
                           **params)
        ip, hostname = parse_network_update_request(r)
        set_network_configuration_for_request(mac_addr, ip=ip, hostname=hostname)
    return {}


def start_background_task_cleaner():  # pragma: nocover
    thread = threading.Thread(target=clean_old_tasks, daemon=True)
    thread.start()


def start_background_mars_poller(mars_host):  # pragma: nocover
    thread = threading.Thread(
        target=mars_poller,
        args=(app, mars_host,),
        daemon=True)
    thread.start()


def create_app(test_config=None):  # pragma: nocover
    if test_config:
        app.config.update(test_config)

    production_only = not app.config['TESTING']
    if production_only:
        start_background_task_cleaner()

        mars_host = app.config.get('MARS_HOST', None)
        if mars_host:
            if not mars_host.startswith('http://'):
                mars_host = 'http://' + mars_host
            logger.info("MARS_HOST is %s, going to poll it...", mars_host)
            start_background_mars_poller(mars_host)

    return app


def main():  # pragma: nocover
    # TODO: Absorb the entrypoint logic
    pass


if __name__ == '__main__':  # pragma: nocover
    main()
